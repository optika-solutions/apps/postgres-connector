import sqlalchemy as sa
import os
from collections import Counter
import shutil


def akumen(**kwargs):
    """
    Parameters:
        - Input: connection_string [string]
        - Input: table [string]
        - Input: query [string]

        - Output: data [file] (data.csv)
        - Output: data_size [float]
    """
    
    if not kwargs.get('connection_string'):
        raise Exception("No connection string was specified, should be of format: postgresql://<username>:<password>@<host>:<port>/<database>")
    
    if not kwargs.get('table') and not kwargs.get('query'):
        raise Exception("One of [query] or [table] should be specified.")
        
    engine = sa.create_engine(kwargs.get('connection_string'))
    
    # we write straight to the disk here rather than serialising from pandas to save memory and cpu
    with open('outputs/data.csv', 'w') as data_file:
        # if both are specified, use query, else use table
        if kwargs.get('query').strip():
            copy_sql = f'COPY ({kwargs.get("query")}) TO STDOUT WITH CSV HEADER'
        else:
            copy_sql = f'COPY {kwargs.get("table")} TO STDOUT WITH CSV HEADER'
        
        conn = engine.raw_connection()
        cur = conn.cursor()
        cur.copy_expert(copy_sql, data_file)
        
    # ensure there aren't duplicate column names
    duplicates = False
    with open('outputs/data.csv') as data_file:
        column_names = data_file.readline().split(',')
        
        counts = Counter(column_names)
        for s,num in counts.items():
            if num > 1: 
                duplicates = True
                for suffix in range(1, num + 1): 
                    column_names[column_names.index(s)] = s + str(suffix) 
                    
        line = ','.join(column_names) + '\n'
    
        # only replace if there's dupe names
        if duplicates:
            with open('outputs/data.csv', 'w') as data_out:
                data_out.write(line)
                shutil.copyfileobj(data_file, data_out)
    
    return {
        'data_size': os.path.getsize('outputs/data.csv')
    }